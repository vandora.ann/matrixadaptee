﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using MatrixSumma;

namespace MatrixGenerator
{
    public class Generator : IOperation
    {
        private string _pathData;
        private string _pathResult;

        public Generator(string pathData, string pathResult)
        {
            _pathData = pathData;
            _pathResult = pathResult;
        }

        public void GenerationMatrixAndWriteToFile(int matM, int matN)
        {
            var mats = new string[2];
            mats[0] = MatrixToString(matM, matN, GeneratorMatrix(matM * matN, 42));
            mats[1] = MatrixToString(matM, matN, GeneratorMatrix(matM * matN, 33));
            FileHelper.WriteMatrixToFile(mats, _pathData);
        }

        public void Summa()
        {
            var adaptee = new Adaptee(_pathData, _pathResult);
            adaptee.Summa();
        }

        private static string MatrixToString(int m, int n, double[] data)
        {
            var result = new StringBuilder();

            result.Append(m.ToString(CultureInfo.InvariantCulture));
            result.Append(" ");
            result.AppendLine(n.ToString(CultureInfo.InvariantCulture));

            for (var i = 0; i < data.Length; ++i)
            {
                result.Append(data[i].ToString(CultureInfo.InvariantCulture));
                result.Append(" ");
            }

            return result.ToString().Trim();
        }
        private static double[] GeneratorMatrix(int size, int start)
        {
            var array = new double[size];
            var rnd = new Random(start);
            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 6 };
            Parallel.For(0, size, parallelOptions, i => array[i] = Math.Round(rnd.Next(-100, 100) + rnd.NextDouble(), 2));
            return array;
        }
    }
}
