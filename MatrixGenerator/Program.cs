﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MatrixAdapter.Tests")]

namespace MatrixGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var pathInput = "C:\\Work\\OTUS\\MatrixAdapter\\input.txt";
            var pathResult = "C:\\Work\\OTUS\\MatrixAdapter\\result.txt";
            var generator = new Generator(pathInput, pathResult);
            generator.GenerationMatrixAndWriteToFile(7, 3);
            generator.Summa();
        }
    }
}
