﻿using System.IO;
using System.Text;

namespace MatrixGenerator
{
    internal static class FileHelper
    {
        public static void WriteMatrixToFile(string[] mats, string pathResult)
        {
            var data = new StringBuilder();

            foreach (var mat in mats)
            {
                data.AppendLine(mat);
            }

            if (File.Exists(pathResult))
                File.Delete(pathResult);

            File.WriteAllText(pathResult, data.ToString());
        }
    }
}
