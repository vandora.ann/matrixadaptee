﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MatrixAdapter.Tests")]

namespace MatrixSumma
{
    class Program
    {
        static void Main(string[] args)
        {
            var adaptee = new Adaptee("C:\\Work\\OTUS\\MatrixAdapter\\input.txt", "C:\\Work\\OTUS\\MatrixAdapter\\result.txt");
            adaptee.Summa();
        }
    }
}
