﻿namespace MatrixSumma
{
    internal class InputData
    {
        public Matrix Matrix1 { get; }
        public Matrix Matrix2 { get; }

        public InputData(Matrix matrix1, Matrix matrix2)
        {
            Matrix1 = matrix1;
            Matrix2 = matrix2;
        }
    }
}
