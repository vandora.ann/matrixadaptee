﻿using System;
using System.IO;
using System.Globalization;
using System.Text;

namespace MatrixSumma
{
    internal class MatrixHelper
    {
        public static InputData ReadData(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();
            var lines = File.ReadAllLines(path);
            if (lines.Length != 4)
                throw new Exception("Файл не содержит корректной матрицы");

            return new InputData(ReadMatrix(lines[0], lines[1]), ReadMatrix(lines[2], lines[3]));
        }

        public static void WriteResult(Matrix matrix, string path)
        {
            var result = new StringBuilder();

            result.Append(matrix.M.ToString(CultureInfo.InvariantCulture));
            result.Append(" ");
            result.AppendLine(matrix.N.ToString(CultureInfo.InvariantCulture));

            for (var i = 0; i < matrix.M; ++i)
            {
                for (var j = 0; j < matrix.N; ++j)
                {
                    result.Append(matrix.GetElement(i, j).ToString(CultureInfo.InvariantCulture));
                    result.Append(" ");
                }
            }

            if (File.Exists(path))
                File.Delete(path);

            File.WriteAllText(path, result.ToString().Trim());
        }

        private static Matrix ReadMatrix(string size, string data)
        {
            var dims = size.Split(' ');
            if (dims.Length < 1)
                throw new Exception("Файл не содержит корректной матрицы");

            try
            {
                var m = Convert.ToInt32(dims[0]);
                var n = dims.Length == 1 ? m : Convert.ToInt32(dims[1]);
                var count = n * m;
                var dataArray = data.Split(' ');
                if (dataArray.Length < count)
                    throw new Exception("Файл не содержит корректной матрицы");

                var dataMat = new double[count];

                for (var i = 0; i < count; ++i)
                {
                    dataMat[i] = Convert.ToDouble(dataArray[i], CultureInfo.InvariantCulture);
                }

                return new Matrix(m, n, dataMat);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
