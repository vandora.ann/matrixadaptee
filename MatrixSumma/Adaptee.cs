﻿namespace MatrixSumma
{
    public class Adaptee : IOperation
    {
        private string _pathInput;
        private string _pathResult;

        public Adaptee(string pathInput, string pathResult)
        {
            _pathInput = pathInput;
            _pathResult = pathResult;
        }

        public void Summa()
        {
            var inputData = MatrixHelper.ReadData(_pathInput);
            var matrix = inputData.Matrix1 + inputData.Matrix2;
            MatrixHelper.WriteResult(matrix, _pathResult);
        }
    }
}
