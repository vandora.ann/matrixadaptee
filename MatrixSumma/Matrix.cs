﻿using System;
using System.Threading.Tasks;

namespace MatrixSumma
{
    internal class Matrix
    {
        private readonly double[] _data;
        private readonly int _m;
        private readonly int _n;

        public int M => _m;

        public int N => _n;

        public bool IsSquare => _m == _n;

        public Matrix(int m, int n, double[] data)
        {
            if (data.Length != m * n)
                throw new ArgumentException("Неверная размерность матрицы");

            _m = m;
            _n = n;
            _data = data;
        }

        public double GetElement(int i, int j)
        {
            return _data[i * _n + j];
        }

        public void SetElement(int i, int j, double element)
        {
            _data[i * _n + j] = element;
        }

        public bool IsEquals(Matrix matrix)
        {
            if (_m != matrix.M)
                return false;

            if (_n != matrix.N)
                return false;

            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 4 };
            var result = true;
            Parallel.For(0, _n, parallelOptions, i =>
            {
                var line = i * _n;
                Parallel.For(0, _n, parallelOptions, async j =>
                {
                    if (Math.Abs(_data[line + j] - matrix._data[line + j]) > 0.00001)
                        result = false;
                });
            });
            return result;
        }

        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1.N != matrix2.N || matrix1.M != matrix2.M)
                throw new ArgumentException("Матрицы разных размерностей");
            
            var count = matrix1.N * matrix1.M;
            var data = new double[count];

            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 4 };

            Parallel.For(0, count, parallelOptions, j => data[j] = matrix1._data[j] + matrix2._data[j]);

            return new Matrix(matrix1.M, matrix1.N, data);
        }
    }
}
