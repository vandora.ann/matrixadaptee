using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MatrixAdapter.Tests
{
    [TestClass]
    public class MatrixTests
    {
        [TestMethod]
        public void Summa()
        {
            var mat1 = new MatrixSumma.Matrix(2, 3, new double[] { 1, 2, 0, 2, -1, -3 });
            var mat2 = new MatrixSumma.Matrix(2, 3, new double[] { 3, 2, 4, 2, 5, 7 });
            var matT = new MatrixSumma.Matrix(2, 3, new double[] { 4, 4, 4, 4, 4, 4 });

            Assert.IsTrue(matT.IsEquals(mat1 + mat2));
        }
    }
}
