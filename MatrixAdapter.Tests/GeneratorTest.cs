﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MatrixGenerator;

namespace MatrixAdapter.Tests
{
    [TestClass]
    public class GeneratorTest
    {
        [TestMethod]
        public void Generator()
        {
            var pathData = Path.Combine(AppContext.BaseDirectory, "Data.txt");
            var generator = new Generator(pathData, null);
            generator.GenerationMatrixAndWriteToFile(4, 7);
            Assert.IsTrue(File.Exists(pathData));

            var lines = File.ReadAllLines(pathData);

            Assert.AreEqual(lines[0].Split(' ').Length, 2);
            Assert.AreEqual(lines[1].Split(' ').Length, 28);
            Assert.AreEqual(lines[2].Split(' ').Length, 2);
            Assert.AreEqual(lines[3].Split(' ').Length, 28);
        }

        [TestMethod]
        public void Adapter()
        {
            var pathData = Path.Combine(AppContext.BaseDirectory, "Data.txt");
            var pathResul = Path.Combine(AppContext.BaseDirectory, "Resul.txt");
            var generator = new Generator(pathData, pathResul);
            generator.GenerationMatrixAndWriteToFile(4, 7);
            generator.Summa();
            Assert.IsTrue(File.Exists(pathResul));

            var lines = File.ReadAllLines(pathResul);

            Assert.AreEqual(lines[0].Split(' ').Length, 2);
            Assert.AreEqual(lines[1].Split(' ').Length, 28);
        }
    }
}
